<?php

namespace AdminBundle\Tests;

use AdminBundle\Action\Crud\EditAction;
use AdminBundle\Model\ActionManager;
use AdminBundle\Tests\Fixtures\DummyAction;
use PHPUnit\Framework\TestCase;

class TestActionManager extends TestCase
{
    public function testAddAction()
    {
        $manager = new ActionManager();
        $manager->setConfiguration(['test' => []]);
        $manager->addAction('test', new DummyAction());

        $this->assertInstanceOf(DummyAction::class, $manager->getAction('test'));
    }

    public function testAddActionException()
    {
        $manager = new ActionManager();
        $manager->setConfiguration(['test' => []]);

        $manager->addAction('test', new DummyAction());

        $this->expectException(\InvalidArgumentException::class);
        $manager->addAction('test', new DummyAction());
    }
}