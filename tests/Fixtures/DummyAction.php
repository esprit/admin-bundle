<?php

namespace AdminBundle\Tests\Fixtures;

use AdminBundle\Action\AbstractAction;
use Symfony\Component\HttpFoundation\Response;

class DummyAction extends AbstractAction
{
    /**
     * @return array
     */
    public function getRequestAttributes()
    {
        return [];
    }

    public function execute($configuration)
    {
        return new Response();
    }
}