<?php

namespace AdminBundle\Action;

class Configuration
{
    protected $configuration = [];
    
    public function get($key, $default = null)
    {
        return isset($this->configuration[$key])
            ? $this->configuration[$key]
            : $default;
    }

    public function has($key)
    {
        return isset($this->configuration[$key]);
    }

    public function set($key, $value)
    {
        return $this->configuration[$key] = $value;
    }
    
    /**
     * @param array $configuration
     * @return $this
     */
    public function setConfiguration(array $configuration): static
    {
        $this->configuration = array_merge($this->configuration, $configuration);

        return $this;
    }
}