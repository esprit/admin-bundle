<?php

namespace AdminBundle\Action\Config;

use AdminBundle\Action\Configuration;
use AdminBundle\Action\Crud\ListAction;
use AdminBundle\Action\Crud\ShowAction;
use Symfony\Component\PropertyAccess\PropertyAccess;

class FieldConfig extends Configuration
{
    private $name;

    /** @var ListAction|ShowAction */
    private $parent;

    protected $configuration = [];

    public function __construct($name, $config, $parent)
    {
        // use string in confuguration as a label
        if (is_string($name) and is_string($config)) {
            $newConfig['label'] = $config;
            $config = $newConfig;
        }

        if (is_string($config)) {
            $name = $config;
            $config = [];
        }

        if (preg_match('/([^\|]+)\|(.+)/', $name, $matches)) {
            $name = $matches[1];
            $config['filter'] = $matches[2];
        }

        if (preg_match('/(.+)~$/', $name, $matches)) {
            $name = $matches[1];
            $config['sortable'] = true;
        }

        if (preg_match('/^([^@]+)@(.+)/', $name, $matches)) {
            $name = $matches[1];
            $config['route'] = $matches[2];
        }

        $this->name = $name;
        $this->parent = $parent;

        parent::setConfiguration($config);
    }

    public function getLabel()
    {
        if ($this->has('label')) {
            return $this->get('label');
        }

        if ($this->parent->has('label')) {
            $label = $this->parent->parseString($this->parent->get('label'));
            return strtr($label, [
                '%property%' => $this->name,
            ]);
        }

        return ucfirst($this->getName());
    }

    public function isSortable()
    {
        if (! $this->parent->get('pagination')) {
            return false;
        }

        return $this->get('sortable');
    }

    public function getTemplate()
    {
        return $this->get('template');
    }

    public function getRoute()
    {
        return $this->get('route');
    }

    /**
     * @param mixed $entity
     * @return array|null
     */
    public function getRouteConfig($entity): ?array
    {
        if (! $route = $this->get('route')) {
            return null;
        }

        $params = $entity;

        $value = $this->getValue($entity);

        if ($value instanceof \Traversable) {
            $config = [];
            foreach ($value as $item) {
                $config[] = [
                    'name' => $route,
                    'params' => $item,
                    'text' => $item,
                ];
            }

            return $config;
        }

        if (is_object($value) and !($value instanceof \DateTime)) {
            $params = $value;
        }

        return [
            'name' => $route,
            'params' => $params,
            'text' => $value,
        ];
    }

    public function getName()
    {
        return $this->name;
    }

    public function getAlias()
    {
        return sprintf('%s.%s', $this->parent->getRootAlias(), $this->name);
    }

    /**
     * @param mixed $parent
     */
    public function setParent($parent)
    {
        $this->parent = $parent;
    }

    public function getFilter()
    {
        return $this->get('filter');
    }

    public function getValue($entity)
    {
        $value = $this->get('value');
        if (is_callable($value)) {
            return call_user_func($value, $entity);
        }

        $accessor = PropertyAccess::createPropertyAccessor();
        return $accessor->getValue($entity, $this->getName());
    }

    public function getFormType()
    {
        return $this->get('type');
    }

    public function getFormOptions()
    {
        return $this->get('options', []);
    }

    public function getType($entity)
    {
        $value = $this->getValue($entity);

        if ($value instanceof \DateTimeInterface) {
            return 'datetime';
        }

        return gettype($value);
    }
}
