<?php

namespace AdminBundle\Action;

use AdminBundle\Action\Crud\RepositoryAction;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

abstract class EntityAction extends RepositoryAction
{
    /** @var UrlGeneratorInterface */
    protected $router;

    public function __construct(EntityManager $entityManager, UrlGeneratorInterface $router)
    {
        $this->router = $router;
        parent::__construct($entityManager);
    }

    protected function getEntityFromRequest()
    {
        $class = $this->getRepository()->getClassName();
        foreach ($this->getRequest()->attributes->all() as $attribute) {
            if ($attribute instanceof $class) {
                return $attribute;
            }
        }

        if ($id = $this->getRequest()->get('id')) {
            if ($entity = $this->entityManager->find($this->getClassName(), $id)) {
                return $entity;
            }
        }

        throw new \LogicException('Cant get entity from request');
    }

    protected function createRedirectUrl($entity, $default = '@index')
    {
        $redirect = $this->get('redirect');

        if (! $redirect) {
            // TODO check if routing exists or catch RouteNotFoundException
            $redirect = $this->router->generate($default);
        }

        if (preg_match('/^[^\/]/', $redirect)) {
            $redirect = $this->router->generate($redirect, $entity);
        }

        return $redirect;
    }
}