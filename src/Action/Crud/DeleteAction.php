<?php

namespace AdminBundle\Action\Crud;

use AdminBundle\Action\EntityAction;
use Doctrine\ORM\EntityManager;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class DeleteAction extends EntityAction
{
    protected $requestStack;

    public function __construct(EntityManager $entityManager, RequestStack $requestStack, UrlGeneratorInterface $router)
    {
        $this->requestStack = $requestStack;
        parent::__construct($entityManager, $router);
    }

    private function getSession(): Session
    {
        return $this->requestStack->getSession();
    }

    public function getRequestAttributes(): array
    {
        return [];
    }

    protected function getExceptionMessage($exceptionObject = null)
    {
        $exception = $this->get('exception');

        if (! $exception) {
            return null;
        }

        if (is_string($exception)) {
            return $this->parseString($exception);
        }

        if (is_array($exception)) {

            // TODO use is_a
            $class = get_class($exceptionObject);
            return $this->parseString($exception[$class]);
        }

        throw new \Exception('Can not resolve exception');
    }

    public function execute($configuration): RedirectResponse
    {
        $this->setConfiguration($configuration);

        $entity = $this->getEntityFromRequest();

        try {
            $this->entityManager->remove($entity);
            $this->entityManager->flush();

            // TODO dispatch event here

            if ($flash = $this->getFlash()) {
                $this->getSession()->getFlashBag()->add('success', $flash);
            }
        } catch (\Exception $e) {

            $exceptionMessage = $this->getExceptionMessage($e);
            if (null === $exceptionMessage) {
                throw $e;
            }

            $this->getSession()->getFlashBag()->add('danger', $exceptionMessage);
        }
        return new RedirectResponse($this->createRedirectUrl($entity), 302);
    }
}