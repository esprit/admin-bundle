<?php

namespace AdminBundle\Action\Crud;

use AdminBundle\Action\EntityAction;
use Symfony\Component\HttpFoundation\Response;

class ShowAction extends EntityAction
{
    public function getRequestAttributes() : array
    {
        return [];
    }

    public function execute($configuration): array|Response
    {
        $this->setConfiguration($configuration);

        return [
            'show' => $this->getFields(),
            'title' => $this->getTitle(),
            'layout' => $this->getLayout(),
            'entity' => $this->getEntityFromRequest(),
        ];
    }
}