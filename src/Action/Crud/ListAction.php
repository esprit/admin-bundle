<?php

namespace AdminBundle\Action\Crud;

use Symfony\Component\HttpFoundation\Response;
use AdminBundle\Form\SearchType;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\QueryBuilder;
use Pagerfanta\Adapter\AdapterInterface;
use Pagerfanta\Doctrine\ORM\QueryAdapter;
use Pagerfanta\Pagerfanta;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Session\SessionInterface;

class ListAction extends RepositoryAction
{
    /** @var FormFactory */
    private $formFactory;

    private $requestStack;

    protected $configuration = [
        'options' => [],
        'pages' => 10,
        'pagination' => true,
        'search' => false,
    ];

    /** @var QueryBuilder */
    protected $queryBuilder;

    /** @var Form */
    protected $search;

    public function __construct(EntityManager $entityManager, FormFactory $formFactory, RequestStack $requestStack)
    {
        $this->formFactory = $formFactory;
        $this->requestStack = $requestStack;
        parent::__construct($entityManager);
    }

    private function getSession(): SessionInterface
    {
        return $this->requestStack->getSession();
    }

    private function parseSort()
    {
        if (! $this->has('sort')) {
            return $this;
        }

        list($name, $order) = $this->get('sort');
        $order = strtoupper($order);
        if ($order != 'DESC') {
            $order = 'ASC';
        }

        // check query builder before geRootAlias
        $queryBuilder = $this->getQueryBuilder();
        if (!$queryBuilder) {
            throw new \RuntimeException('Cant apply sort because QueryBuilder is not set');
        }

        $alias = $this->getRootAlias();
        if (false === strpos($name, '.') and $alias) {
            $name = $alias . '.' . $name;
        }

        $queryBuilder->addOrderBy($name, $order);

        return $this;
    }

    private function getQueryBuilderResult()
    {
        $qb = $this->get('query_builder');

        if ($qb instanceof Collection) {
            return $qb;
        }

        return $this->getQueryBuilder()->getQuery()->getResult();
    }

    private function getQueryBuilder()
    {
        if (! $this->queryBuilder) {
            $this->queryBuilder = $this->createQueryBuilder();
        }

        return $this->queryBuilder;
    }

    private function createQueryBuilder()
    {
        if (! $this->has('query_builder')) {
            return $this->getRepository()->createQueryBuilder('r');
        }

        $qb = $this->get('query_builder');

        if ($qb instanceof QueryBuilder) {
            return $qb;
        }

        if ($qb instanceof EntityRepository) {
            return $qb->createQueryBuilder('r');
        }

        if (is_callable($qb)) {

            return call_user_func(
                $qb,
                $this->getRepository(),
                $this->get('search') ? $this->getSearchForm()->getData() : null);
        }

        //throw new \Exception('Don\'t know how to create query builder');
    }

    public function getRootAlias()
    {
        return $this->getQueryBuilder()->getRootAliases()[0];
    }

    public function getPaginator()
    {
        if (false === ($pagination = $this->get('pagination'))) {
            return $this->getQueryBuilderResult();
        }

        $query = $this->get('query_builder');

        if ($query instanceof QueryAdapter) {
            return $this->createPaginator($query, $this->getRequest());
        }

        $adapter = new QueryAdapter($this->getQueryBuilder(), $pagination['fetchJoinCollection'] ?? true);
        return $this->createPaginator($adapter, $this->getRequest());
    }

    public function handleSearchForm()
    {
        if (! $this->getSearchForm()) {
            return $this;
        }

        if ($this->getRequest()->isMethod('get') and $filters = $this->getFilters()) {
            $this->search->submit($filters);
        }
        else {
            $this->search->handleRequest($this->getRequest());
        }

        if ($this->search->isSubmitted() and $this->search->isValid()) {

            $data = $this->search->getData();

            $this->setFilters($this->getSearchViewData());

            if (is_array($search = $this->get('search'))) {

                $qb = $this->getQueryBuilder();

                $orX = $qb->expr()->orX();
                foreach ($search as $field) {
                    if (false === strpos($field, '.')) {
                        $field = $this->getRootAlias() . '.' . $field;
                    }

                    $orX->add(
                        $qb->expr()->like(
                            $field, $qb->expr()->literal('%' . $data['q'] . '%')
                        ));
                };

                $qb->andWhere($orX);
            }
        }

        return $this;
    }

    public function getSearchViewData()
    {
        $data = [];
        foreach ($this->search as $field) {
            // TODO make it recursive
            $data[$field->getName()] = $field->getViewData();
        }

        return $data;
    }

    public function getRequestAttributes(): array
    {
        return [
            'repository' => $this->getRepository(),
        ];
    }

    public function getSearchForm()
    {
        if (! $this->search) {

            if (! $search = $this->get('search')) {
                return null;
            }

            if ($search instanceof Form) {

                // $this->search should be created before getFilters call
                $this->search = $search;

                // set defaults on existing form
                if ($filters = $this->getFilters()) {
                    $this->search->setData($filters);
                }

                return $this->search;
            }

            if (! is_string($search)) {
                $search = SearchType::class;
            }

            $this->search = $this->formFactory->create($search);
            if ($filters = $this->getFilters()) {
                $this->search->setData($filters);
            }

            return $this->search;
        }

        return $this->search;
    }

    public function saveSearch()
    {
        return $this->get('session', false);
    }

    public function getSearchSession()
    {
        $session = $this->get('session', null);

        if (true === $session) {
            return $this->getPrefix();
        }

        if (is_null($session)) {
            $method = $this->search->getConfig()->getOption('method');
            if (strtoupper($method) == 'POST') {
                // TODO or use form name?
                return $this->getPrefix();
            }
        }

        return $session;
    }

    public function setFilters($data)
    {
        if ($session = $this->getSearchSession()) {
            $this->getSession()->set(sprintf('filter.%s', $session), $data);
        }

        return $this;
    }

    public function getFilters()
    {
        if ($session = $this->getSearchSession()) {
            return $this->getSession()->get(sprintf('filter.%s', $this->getPrefix()));
        }

        return null;
    }

    private function createPaginator(AdapterInterface $adapter, Request $request)
    {
        $paginator = new Pagerfanta($adapter);
        $paginator->setMaxPerPage($this->get('pages'));
        $paginator->setCurrentPage($request->get('page', 1));

        return $paginator;
    }

    public function execute($configuration): array|Response
    {
        $this
            ->setConfiguration($configuration)
            // first we need create and handle search form
            // because query_builder callback have search data as argument
            ->handleSearchForm();

        $paginator = $this->getPaginator();
        $this->parseSort();

        return $this->mergeVars([
            'title' => $this->getTitle(),
            'paginator' => $paginator,
            'list' => $this->getFields(),
            'search' => $this->getSearchForm() ? $this->getSearchForm()->createView() : null,
            'layout' => $this->getLayout(),
        ]);
    }
}
