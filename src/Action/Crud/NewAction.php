<?php

namespace AdminBundle\Action\Crud;

use AdminBundle\Events\FormPostBindEvent;
use AdminBundle\Events\FormPreBindEvent;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\Response;

class NewAction extends EditAction
{
    public function getRequestAttributes(): array
    {
        return [];
    }

    public function execute($configuration): array|Response
    {
        $this->setConfiguration($this->preParseConfig($configuration));

        $form = $this->createForm();
        $request = $this->getRequest();

        $event = new FormPreBindEvent($form, $request);
        $this->dispatcher->dispatch($event);
        if ($response = $event->getResponse()) {
            return $response;
        }

        if (! $form->isSubmitted()) {
            $form->handleRequest($request);
        }

        $event = new FormPostBindEvent($form, $request);
        $this->dispatcher->dispatch($event);
        if ($response = $event->getResponse()) {
            return $response;
        }

        if ($form->isSubmitted() and $form->isValid()) {

            if ($save = $this->get('save') and is_callable($save)) {
                if ($result = call_user_func($save, $form->getData())) {
                    if ($result instanceof Response) {
                        return $result;
                    }

                    if (is_object($result)) {
                        $entity = $result;
                    }
                }
            }

            if (! isset($entity)) {
                $entity = $form->getData();
            }

            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            // TODO dispatch event here

            if ($flash = $this->getFlash()) {
                $this->getSession()->getFlashBag()->add('success', $flash);
            }

            return new RedirectResponse($this->createRedirectUrl($entity), 302);
        }

        return $this->mergeVars([
            'title' => $this->getTitle(),
            'layout' => $this->getLayout(),
            'form' => $form->createView(),
        ]);
    }
}