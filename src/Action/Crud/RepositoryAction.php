<?php

namespace AdminBundle\Action\Crud;

use AdminBundle\Action\AbstractAction;
use AdminBundle\Action\Config\FieldConfig;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;

abstract class RepositoryAction extends AbstractAction
{
    /** @var EntityManager */
    protected $entityManager;

    /** @var EntityRepository */
    protected $repository;

    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    // TODO сделать настройку необязательной для profile
    protected function getRepository()
    {
        if (! $this->repository) {
            $this->repository = $this->entityManager->getRepository($this->get('repository'));
        }

        return $this->repository;
    }

    protected function getClassName()
    {
        $repository = $this->getRepository();

        if (! $repository) {
            return null;
        }

        return $this->getRepository()->getClassName();
    }

    protected function getClassShortName()
    {
        return (new \ReflectionClass($this->getClassName()))->getShortName();
    }

    protected function tableize(string $word) : string
    {
        $tableized = preg_replace('~(?<=\\w)([A-Z])~u', '_$1', $word);

        if ($tableized === null) {
            throw new \RuntimeException(sprintf(
                'preg_replace returned null for value "%s"',
                $word
            ));
        }

        return mb_strtolower($tableized);
    }

    public function getPrefix()
    {
        $default = self::tableize($this->getClassShortName());

        return $this->get('prefix', $default);
    }

    protected function getLayout()
    {
        return $this->get('layout');
    }

    /**
     * @return FieldConfig[]
     */
    public function getFields(): array
    {
        $fields = $this->get('fields');

        if (null === $fields) {

            $className = $this->getRepository()->getClassName();

            // get all preperties from class
            $reflect = new \ReflectionClass($className);
            $props   = $reflect->getProperties();

            $fields = array_map(function($r){ return $r->getName(); }, $props);
        }

        $list = [];
        foreach ($fields as $key => $value) {
            $list[] = new FieldConfig($key, $value, $this);
        }

        return $list;
    }

    public function parseString($string)
    {
        //TODO сделать все подстановки необязательными в случае если не указан класс в repository
        return strtr($string, [
            '%class%' => $this->getClassShortName(),
            '%prefix%' => $this->getPrefix(),
            '%action%' => $this->get('value')
        ]);
    }

    protected function getTitle()
    {
        $title = $this->get('title');

        if (! $title) {
            return null;
        }

        return $this->parseString($title);
    }

    protected function getFlash()
    {
        $flash = $this->get('flash');

        if (! $flash) {
            return null;
        }

        return $this->parseString($flash);
    }
}