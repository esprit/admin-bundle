<?php
namespace AdminBundle\Action\Crud;

use AdminBundle\Action\Config\FieldConfig;
use AdminBundle\Action\EntityAction;
use AdminBundle\Events\FormPostBindEvent;
use AdminBundle\Events\FormPreBindEvent;
use Doctrine\ORM\EntityManager;
use FormGenerator\Generator\FormGenerator;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormFactory;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;

class EditAction extends EntityAction
{
    protected $requestStack;

    /** @var FormFactory */
    protected $formFactory;

    /**
     * @var EventDispatcherInterface
     */
    protected $dispatcher;

    /**
     * @var FormGenerator
     */
    protected $forGenerator;

    public function __construct(EntityManager $entityManager, RequestStack $requestStack, FormFactory $formFactory,
                                UrlGeneratorInterface $router, EventDispatcherInterface $dispatcher, FormGenerator $generator)
    {
        parent::__construct($entityManager, $router);
        $this->requestStack = $requestStack;
        $this->formFactory = $formFactory;
        $this->dispatcher = $dispatcher;
        $this->forGenerator = $generator;
    }

    protected function getSession(): Session
    {
        return $this->requestStack->getSession();
    }

    public function getRequestAttributes(): array
    {
        // TODO: Implement getRequestAttributes() method.
        return [];
    }

    protected function autoGenerateFormTypeClass()
    {
        return 'AppBundle\\Form\\' . $this->getClassShortName() . 'Type';
    }

    protected function createForm($object = null)
    {
        $form = $this->get('form');

        if ($form instanceof Form) {
            return $form;
        }

        if (is_string($form)) {
            if (class_exists($form)) {
                // create form from Type
                if (is_subclass_of($form, AbstractType::class)) {
                    return $this->formFactory->create($form, $object);
                }
                // create form from DTO
                // TODO check if it is DTO
                $form = new $form;
                // fill in form with data
                if ($object and method_exists($object, 'export')) {
                    $form = $object->export($form);
                }
            }
        }

        $entityClass = $this->getRepository()->getClassName();

        if (is_object($form)) {

            // it is DTO?
            if (get_class($form) !== $entityClass) {
                return $this->forGenerator->create($form);
            }

            $object = $form;
        }

        $formClass = $this->autoGenerateFormTypeClass();
        if (class_exists($formClass) and is_subclass_of($formClass, AbstractType::class)) {
            return $this->formFactory->create($formClass, $object);
        }

        if (is_null($object)) {
            // we need to create an object, to enable field types guesser
            $object = new $entityClass;
        }

        // create form from fields
        $formBuilder = $this->formFactory->createNamedBuilder($this->getPrefix(), FormType::class, $object);
        $fields = $this->getFields();

        foreach ($fields as $field) {

            // skip id
            if ('id' == $field->getName()) {
                continue;
            }

            /** @var FieldConfig $field */
            //$formBuilder->add($field->getName(), null, ['label' => $field->getLabel()]);
            $formBuilder->add($field->getName(), $field->getFormType(), $field->getFormOptions());
        }

        return $formBuilder->getForm();
    }

    protected function preParseConfig($configuration)
    {
        $config = [];
        if (is_callable($configuration)) {
            $config['save'] = $configuration;
        } else {
            $config = $configuration;
        }

        return $config;
    }

    public function execute($configuration): array|Response
    {
        $this->setConfiguration($this->preParseConfig($configuration));

        $entity = $this->getEntityFromRequest();
        $form = $this->createForm($entity);

        $request = $this->getRequest();

        $event = new FormPreBindEvent($form, $request);
        $this->dispatcher->dispatch($event);
        if ($response = $event->getResponse()) {
            return $response;
        }

        $form->handleRequest($request);

        $event = new FormPostBindEvent($form, $request);
        $this->dispatcher->dispatch($event);
        if ($response = $event->getResponse()) {
            return $response;
        }

        if ($form->isSubmitted() and $form->isValid()) {

            if ($save = $this->get('save') and is_callable($save)) {
                if ($result = call_user_func($save, $form->getData())) {
                    if ($result instanceof Response) {
                        return $result;
                    }

                    if (is_object($result)) {
                        $entity = $result;
                    }
                }
            }

            if (! isset($entity)) {
                $entity = $form->getData();
            }

            $this->entityManager->persist($entity);
            $this->entityManager->flush();

            // TODO dispatch event here

            if ($flash = $this->getFlash()) {
                $this->getSession()->getFlashBag()->add('success', $flash);
            }

            return new RedirectResponse($this->createRedirectUrl($entity), 302);
        }

        return $this->mergeVars([
            'title' => $this->getTitle(),
            'layout' => $this->getLayout(),
            'form' => $form->createView(),
            'entity' => $entity,
        ]);
    }
}

