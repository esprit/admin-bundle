<?php

namespace AdminBundle\Action;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController as Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

abstract class AbstractAction extends Configuration
{
    /** @var Controller */
    protected $controller;

    /** @var Request */
    private $request;

    /**
     * @return array
     */
    abstract public function getRequestAttributes(): array;

    /**
     * @param array $configuration
     * @return array|Response
     */
    abstract public function execute($configuration): array|Response;

    /**
     * @param mixed $controller
     * @return $this
     */
    public function setController($controller): static
    {
        $this->controller = $controller;

        return $this;
    }

    /**
     * @param Request $request
     * @return $this
     */
    public function setRequest($request): static
    {
        $this->request = $request;

        return $this;
    }
    
    /**
     * @return Request
     */
    public function getRequest() : Request
    {
        return $this->request;
    }

    /**
     * @return array
     */
    public function getTemplateVariables() : array
    {
        return $this->get('vars', $this->configuration[0] ?? []);
    }

    public function mergeVars(array $data): array
    {
        return array_merge($this->getTemplateVariables(), $data);
    }
}
