<?php

namespace AdminBundle\EventListener;

use AdminBundle\Configuration\CrudAction;
use AdminBundle\Model\ActionManager;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ControllerEvent;
use Symfony\Component\HttpKernel\Event\ViewEvent;
use Symfony\Component\HttpKernel\KernelEvents;
use Twig\Environment;

class CrudActionListener implements EventSubscriberInterface
{
    protected $controller;

    private $manager;

    /** @var Environment */
    private $twig;

    public function __construct(ActionManager $manager, Environment $twig)
    {
        $this->manager = $manager;
        $this->twig = $twig;
    }

    public function onKernelController(ControllerEvent $event)
    {
        if (! is_array($controller = $event->getController())) {
            return;
        }

        $request = $event->getRequest();

        if (! $configuration = $this->getConfiguration($request->attributes->get('_crudaction'))) {
            return;
        }

        $controller = $controller[0];

        if (! $configuration->getValue()) {
            return;
        }

        $name = $configuration->getValue();
        $action = $this->manager->getAction($name);

        $attributes = $action
            // TODO надо создавать и инжектмть новый уникальный инстанс конфигурации, а не мержить конфиг в текущий серис!!!
            ->setConfiguration($configuration->toArray())
            ->setController($controller)
            ->getRequestAttributes();

        $request->attributes->add($attributes);
    }

    public function onKernelView(ViewEvent $event)
    {
        $request = $event->getRequest();
        $parameters = $event->getControllerResult();

        if (! $configuration = $this->getConfiguration($request->attributes->get('_crudaction'))) {
            return;
        }

        if (! $configuration->getValue()) {
            return;
        }

        $name = $configuration->getValue();
        $action = $this->manager->getAction($name);
        $action->setRequest($request);

        $result = $action->execute($parameters ?? []);

        if ($result instanceof Response) {
            $event->setResponse($result);

            return;
        }

        if (! $request->attributes->get('_template')) {
            $templateName = sprintf('@Admin/Admin/%s.html.twig', $configuration->getValue());
            $content = $this->twig->render($templateName, $result);
            $result = new Response($content);
            $event->setResponse($result);
            return;
        }

        $event->setControllerResult($result);
    }

    public function getConfiguration($configuration)
    {
        if (! $configuration)
            return;

        if (1 == count($configuration)) {
            return $configuration[0];
        }

        /** @var CrudAction $last */
        $last = end($configuration);
        $currentAction = $last->getValue();

        /** @var CrudAction $c */
        $data = [];
        foreach ($configuration as $c) {
            if (null === $c->getValue() or $c->getValue() === $currentAction) {
                $data = array_merge($data, array_filter($c->toArray()));
            }
        }

        return new CrudAction($data);
    }

    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::CONTROLLER => ['onKernelController', -128],
            KernelEvents::VIEW => ['onKernelView', 128],
        ];
    }
}

