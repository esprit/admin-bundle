<?php


namespace AdminBundle\Twig\Extension;

use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class PhpFunctionExtension extends AbstractExtension
{
    private $functions = [
        'gettype',
    ];

    public function __construct(array $functions = [])
    {
        if ($functions) {
            $this->allowFunctions($functions);
        }
    }
    public function getFunctions()
    {
        $twigFunctions = [];
        foreach ($this->functions as $function) {
            $twigFunctions[] = new TwigFunction($function, $function);
        }
        return $twigFunctions;
    }
    public function allowFunction($function)
    {
        $this->functions[] = $function;
    }
    public function allowFunctions(array $functions)
    {
        $this->functions = $functions;
    }
    public function getName()
    {
        return 'php_function';
    }
}