<?php

namespace AdminBundle\Twig\Extension;

use AdminBundle\Routing\CrudRouter;
use AdminBundle\Routing\CrudRouterInterface;
use AdminBundle\Services\Context;
use Twig\Extension\AbstractExtension;
use Twig\TwigFunction;

class RouteExtension extends AbstractExtension
{
    /**
     * @var Context
     */
    private $context;

    /**
     * @var CrudRouterInterface
     */
    private $router;

    public function __construct(Context $context, CrudRouter $router)
    {
        $this->context = $context;
        $this->router = $router;
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'route';
    }

    public function getFunctions()
    {
        return [
            new TwigFunction('route_exists', [$this, 'routeExists']),
            new TwigFunction('is_route', [$this, 'isRoute']),
            new TwigFunction('admin_path', [$this, 'adminPath']),
        ];
    }

    public function routeExists($name)
    {
        return $this->router->routeExists($name);
    }

    public function isRoute($route)
    {
        return $this->context->isRoute($route);
    }

    public function adminPath(string $routeName, $entity = null)
    {
        return $this->router->generate($routeName, $entity ? $entity : []);
    }
}