<?php

namespace AdminBundle\Twig\Extension;

use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;
use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;

class ApplyFilterExtension extends AbstractExtension
{
    /**
     * @var UrlGeneratorInterface
     */
    private $router;

    /**
     * ApplyFilterExtension constructor.
     * @param UrlGeneratorInterface $router
     */
    public function __construct(UrlGeneratorInterface $router)
    {
        $this->router = $router;
    }

    /**
     * Returns the name of the extension.
     *
     * @return string The extension name
     */
    public function getName()
    {
        return 'apply_filter';
    }

    public function getFilters()
    {
        return [
            new TwigFilter('apply_filter', [$this, 'applyFilter'], [
                    'needs_environment' => true,
                    'needs_context'     => true,
                    'is_safe'           => ['all'],
                ]
            ),
            new TwigFilter('link', [$this, 'link'], [
                'is_safe' => ['all'],
            ]),
        ];
    }

    public function applyFilter(Environment $env, $context, $value, $filters)
    {
        if ($filters) {
            $name = 'apply_filter_' . md5($filters);
            $template = sprintf('{{ %s|%s }}', $name, $filters);
            $template = $env->createTemplate($template);
            $context[$name] = $value;

            return $template->render($context);
        }

        return $value;
    }

    public function link($value, $routeName, $routeParams = [])
    {
        if (! $routeParams) {
            $routeParams = $value;
        }

        return sprintf('<a href="%s">%s</a>',
            $this->router->generate($routeName, $routeParams),
            $value
        );
    }
}