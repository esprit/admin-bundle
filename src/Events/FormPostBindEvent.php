<?php

namespace AdminBundle\Events;

use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Contracts\EventDispatcher\Event;

class FormPostBindEvent extends Event
{
    /**
     * @var Request
     */
    private $request;

    /**
     * @var Form
     */
    private $form;

    private ?Response $response = null;

    public function __construct(FormInterface $form, Request $request)
    {
        $this->request = $request;
        $this->form = $form;
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return $this->request;
    }

    /**
     * @return Form
     */
    public function getForm(): Form
    {
        return $this->form;
    }

    public function getResponse(): ?Response
    {
        return $this->response;
    }

    /**
     * @param Response $response
     */
    public function setResponse($response)
    {
        $this->response = $response;
    }
}