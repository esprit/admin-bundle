<?php

namespace AdminBundle\Services;

use Symfony\Component\HttpFoundation\RequestStack;

class Context
{
    private $requestStack;

    private $action;

    public function __construct(RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;
    }

    public function getAction()
    {
        if (is_null($this->action)) {

            $request = $this->requestStack->getMainRequest();

            $controller = $request->attributes->get('_controller');

            if (! preg_match('/::(.+)Action/', $controller, $matches)) {
                throw new \Exception('Cant get action from _controller ' . $controller);
            }

            $this->action = $matches[1];
        }

        return $this->action;
    }

    public function isAction($action)
    {
        return $this->getAction() == $action;
    }

    public function getRoute()
    {
        return $this->requestStack->getMainRequest()->attributes->get('_route');
    }

    public function isRoute($route)
    {
        if (is_array($route)) {
            foreach ($route as $r) {
                if ($this->isRoute($r)) {
                    return true;
                }
            }

            return false;
        }

        if ($result = @preg_match($route, $this->getRoute()))
        {
            return $result;
        }

        return $this->getRoute() == $route;
    }

    public function getParameter($param_name)
    {
        return $this->requestStack->getMainRequest()->get($param_name);
    }

    public function getPath()
    {
        return $this->requestStack->getMainRequest()->getPathInfo();
    }

    public function isPath($path)
    {
        if (is_array($path)) {
            foreach ($path as $p) {
                if ($this->isPath($p)) {
                    return true;
                }
            }

            return false;
        }

        if ($result = @preg_match($path, $this->getPath()))
        {
            return $result;
        }

        return $this->getPath() == $path;
    }
}
