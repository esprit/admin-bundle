<?php

namespace AdminBundle\Menu;

use AdminBundle\Configuration\Menu;

class MenuNode
{
    /** @var Menu */
    private $menu;

    /** @var bool */
    private $active;

    /** @var MenuNode[] */
    private $items = [];

    /**
     * MenuNode constructor.
     * @param Menu $menu
     */
    public function __construct(Menu $menu = null, bool $active = false)
    {
        $this->menu = $menu;
        $this->active = $active;
    }

    /**
     * @return Menu
     */
    public function getMenu(): Menu
    {
        return $this->menu;
    }

    /**
     * @param MenuNode $node
     * @return $this
     */
    public function addItem(MenuNode $node): static
    {
        $this->items[] = $node;

        return $this;
    }

    /**
     * @return MenuNode[]
     */
    public function getItems(): array
    {
        uasort($this->items, function (MenuNode $a, MenuNode $b){
            return $a->getMenu()->getPosition() <=> $b->getMenu()->getPosition();
        });

        return $this->items;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     */
    public function setActive(bool $active)
    {
        $this->active = $active;
    }
}