<?php

namespace AdminBundle\Menu;

use AdminBundle\Configuration\Menu;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\RouterInterface;

class MenuBuilder
{
    public function __construct(
        private RequestStack $requestStack,
        private RouterInterface $router)
    {
    }

    private function getCurrentController(): string
    {
        $request = $this->requestStack->getMainRequest();
        list($currentController, ) = explode(':', $request->attributes->get('_controller'));

        return $currentController;
    }

    public function getControllers(): array
    {
        $routes = $this->router->getRouteCollection();

        $controllers = [];
        foreach ($routes as $route) {
            [$controller, ] = explode('::', $route->getDefault('_controller'));
            if (class_exists($controller) and !in_array($controller, $controllers)) {
                 $controllers[] = $controller;
            }
        }

        return $controllers;
    }

    /**
     * @return MenuNode
     */
    public function build(): MenuNode
    {
        $controllers = $this->getControllers();
        $currentController = $this->getCurrentController();

        $menu = new MenuNode();
        foreach ($controllers as $class) {
            $ref = new \ReflectionClass($class);
            $attributes = $ref->getAttributes(Menu::class);
            foreach ($attributes as $attribute) {
                /** @var Menu $conf */
                $conf = $attribute->newInstance();
                $menu->addItem(new MenuNode($conf, $currentController === $class));
            }
        }

        return $menu;
    }
}