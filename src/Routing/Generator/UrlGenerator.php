<?php

namespace AdminBundle\Routing\Generator;

use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\Routing\Generator\CompiledUrlGenerator as BaseUrlGenerator;

/**
 * UrlGenerator generates URL based on a set of routes.
 *
 * @api
 */
class UrlGenerator extends BaseUrlGenerator
{
    protected function doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, array $requiredSchemes = []): string
    {
        if (is_object($parameters)) {
            $object = $parameters;
            $parameters = [];
        }

        if (isset($parameters[0]) && is_object($parameters[0])) {
            $object = $parameters[0];
        }

        if (isset($parameters['_subject']) && is_object($parameters['_subject'])) {
            $object = $parameters['_subject'];
        }

        if (isset($object)) {
            $mergedParams = array_replace($defaults, $this->context->getParameters(), $parameters);
            if ($diff = array_diff_key(array_flip($variables), $mergedParams)) {
                $parameters += $this->getParametersFromObject(array_keys($diff), $object);
            }

            unset($parameters[0]);
            unset($parameters['_subject']);
        }

        if (!$parameters) {
            $parameters = [];
        }

        return parent::doGenerate($variables, $defaults, $requirements, $tokens, $parameters, $name, $referenceType, $hostTokens, $requiredSchemes);
    }

    protected function getParametersFromObject($keys, $object)
    {
        $parameters = [];
        $accessor = PropertyAccess::createPropertyAccessor();

        foreach ($keys as $key) {
            $parameters[$key] = $accessor->getValue($object, $key);
        }

        return $parameters;
    }
}
