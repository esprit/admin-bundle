<?php

namespace AdminBundle\Routing;

interface CrudRouterInterface
{
    public function routeExists($name);
}