<?php

namespace AdminBundle\Routing;

use AdminBundle\Routing\Generator\UrlGenerator;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Routing\Matcher\RequestMatcherInterface;
use Symfony\Component\Routing\RequestContext;
use Symfony\Component\Routing\Router;
use Symfony\Component\Routing\RouterInterface;

class CrudRouter implements CrudRouterInterface, RouterInterface, RequestMatcherInterface
{
    private $requestStack;

    private $router;

    public function __construct(Router $router, RequestStack $requestStack)
    {
        $this->requestStack = $requestStack;

        $router->setOption('generator_class', UrlGenerator::class);
        //$router->setOption('generator_base_class', UrlGenerator::class);
        $this->router = $router;
    }

    private function getPrefix(): string
    {
        if (! $currentRouteName = $this->requestStack->getCurrentRequest()->get('_route')) {
            return '';
        }

        list($prefix,) = explode('@', $currentRouteName);

        return $prefix;
    }

    private function getFullRouteName($name): string
    {
        if ($name and '@' === substr($name, 0, 1)) {

            if (! $prefix = $this->getPrefix()) {
                throw new \Exception('Prefix not found!');
            }

            return $prefix . $name;
        }

        return $name;
    }

    public function routeExists($name)
    {
        return $this->router->getRouteCollection()->get($this->getFullRouteName($name)) ? true : false;
    }

    public function generate($name, $parameters = [], $referenceType = self::ABSOLUTE_PATH): string
    {
        if (is_object($parameters)) {
            $params['_subject'] = $parameters;
            $parameters = $params;
        }

        return $this->router->generate($this->getFullRouteName($name), $parameters, $referenceType);
    }

    /**
     * @inheritdoc
     */
    public function setContext(RequestContext $context)
    {
        $this->router->setContext($context);
    }

    /**
     * @inheritdoc
     */
    public function getContext(): RequestContext
    {
        return $this->router->getContext();
    }

    /**
     * @inheritdoc
     */
    public function getRouteCollection()
    {
        return $this->router->getRouteCollection();
    }

    /**
     * @inheritdoc
     */
    public function match($pathinfo): array
    {
        return $this->router->match($pathinfo);
    }

    /**
     * @inheritdoc
     */
    public function matchRequest(Request $request): array
    {
        return $this->router->matchRequest($request);
    }
}