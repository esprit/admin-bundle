<?php

namespace AdminBundle\DependencyInjection\Compiler;

use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;

class ActionPass implements CompilerPassInterface
{
    public function process(ContainerBuilder $container)
    {
        if (!$container->has('admin.action_manager')) {
            return;
        }

        $definition = $container->findDefinition('admin.action_manager');
        $taggedServices = $container->findTaggedServiceIds('action');

        foreach ($taggedServices as $id => $tags) {
            foreach ($tags as $attributes) {
                $definition->addMethodCall('addAction', [
                    $attributes['alias'],
                    new Reference($id)
                ]);
            }
        }
    }
}