<?php

namespace AdminBundle;

use AdminBundle\DependencyInjection\Compiler\ActionPass;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\HttpKernel\Bundle\Bundle;

class AdminBundle extends Bundle
{
    public function build(ContainerBuilder $container)
    {
        $container->addCompilerPass(new ActionPass());
    }
}
