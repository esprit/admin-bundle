<?php

namespace AdminBundle\Model;

use AdminBundle\Action\AbstractAction;
use InvalidArgumentException;

class ActionManager
{
    /** @var AbstractAction[] */
    private $actions = [];

    private $config;

    public function addAction(string $alias, AbstractAction $action)
    {
        if (array_key_exists($alias, $this->actions)) {
            throw new InvalidArgumentException(
                sprintf("Action named '%s' already exists", $alias)
            );
        }

        $this->actions[$alias] = $action;
    }

    public function getAction(string $alias) : AbstractAction
    {
        /** @var AbstractAction $action */
        $action =  $this->actions[$alias];
        $action->setConfiguration($this->config[$alias] ?? []);

        return $action;
    }

    public function setConfiguration(array $config)
    {
        $this->config = $config;
    }
}