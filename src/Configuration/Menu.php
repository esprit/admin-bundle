<?php

namespace AdminBundle\Configuration;

#[\Attribute(\Attribute::TARGET_CLASS)]
class Menu
{
    public function __construct(
        private string $value,
        private string $route,
        private int $position = 0,
        private ?string $icon = null,
    ){}

    /**
     * @param mixed $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @param mixed $route
     */
    public function setRoute($route)
    {
        $this->route = $route;
    }

    /**
     * @return mixed
     */
    public function getLabel(): mixed
    {
        return $this->value;
    }

    /**
     * @return mixed
     */
    public function getRoute(): mixed
    {
        return $this->route;
    }

    /**
     * Returns the alias name for an annotated configuration.
     *
     * @return string
     */
    public function getAliasName(): string
    {
        return 'menu';
    }

    /**
     * Returns whether multiple annotations of this type are allowed.
     *
     * @return bool
     */
    public function allowArray(): bool
    {
        return false;
    }

    /**
     * @return int
     */
    public function getPosition(): int
    {
        return $this->position;
    }

    /**
     * @param int $position
     */
    public function setPosition(int $position)
    {
        $this->position = $position;
    }

    public function getIcon(): ?string
    {
        return $this->icon;
    }

    /**
     * @param string $icon
     */
    public function setIcon(string $icon)
    {
        $this->icon = $icon;
    }
}