<?php

namespace AdminBundle\Configuration;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ConfigurationAnnotation;

/**
 * @Annotation
 */
#[\Attribute(\Attribute::TARGET_CLASS | \Attribute::TARGET_METHOD)]
class CrudAction extends ConfigurationAnnotation
{
    public function __construct(
        protected $data = [],
        protected ?string $action = null,
        protected ?string $form = null,
        protected ?string $entity = null,
        protected ?string $repository = null,
        protected ?string $prefix = null,
        protected ?string $layout = null,
        protected array $fields = [],
        protected ?string $redirect = null,
        protected array $sort = [],
        protected ?string $title = null,
        protected ?string $flash = null,
    ){
        if (is_string($data)) {
            $this->action = $data;
        } else {
            foreach ($data as $key => $value) {
                $method = 'set'.str_replace('_', '', $key);
                if (!method_exists($this, $method)) {
                    throw new \BadMethodCallException(sprintf('Unknown property "%s" on annotation "%s".', $key, static::class));
                }
                $this->$method($value);
            }
        }
    }

    /**
     * @return string
     */
    public function getFlash(): string
    {
        return $this->flash;
    }

    /**
     * @param string $flash
     */
    public function setFlash(string $flash)
    {
        $this->flash = $flash;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle(string $title)
    {
        $this->title = $title;
    }

    public function setValue($value)
    {
        $this->action = $value;
    }

    public function getValue()
    {
        return $this->action;
    }

    public function setForm($form)
    {
        $this->form = $form;
    }

    public function getForm()
    {
        return $this->form;
    }

    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    public function getEntity()
    {
        return $this->entity;
    }

    public function setRepository($repository)
    {
        $this->repository = $repository;
    }

    public function getRepository()
    {
        return $this->repository;
    }

    /**
     * Returns the alias name for an annotated configuration.
     *
     * @return string
     */
    public function getAliasName(): string
    {
        return 'crudaction';
    }

    /**
     * Returns whether multiple annotations of this type are allowed
     *
     * @return bool
     */
    public function allowArray(): bool
    {
        return true;
    }

    public function toArray()
    {
        return array_filter([
            'value' => $this->action ?? null,
            'form' => $this->form ?? null,
            'entity' => $this->entity ?? null,
            'repository' => $this->repository ?? null,
            'prefix' => $this->prefix ?? null,
            'fields' => $this->fields ?? [],
            'redirect' => $this->redirect ?? null,
            'layout' => $this->layout ?? null,
            'title' => $this->title ?? null,
            'flash' => $this->flash ?? null,
            'sort' => $this->sort ?? [],
        ]);
    }

    /**
     * @return mixed
     */
    public function getPrefix(): mixed
    {
        return $this->prefix;
    }

    /**
     * @param mixed $prefix
     */
    public function setPrefix($prefix)
    {
        $this->prefix = $prefix;
    }

    public function getServiceName()
    {
        return $this->getValue();
    }

    /**
     * @return mixed
     */
    public function getLayout(): mixed
    {
        return $this->layout;
    }

    /**
     * @param mixed $layout
     */
    public function setLayout($layout)
    {
        $this->layout = $layout;
    }

    /**
     * @return array
     */
    public function getFields(): array
    {
        return $this->fields;
    }

    /**
     * @param array $fields
     */
    public function setFields($fields)
    {
        $this->fields = $fields;
    }

    /**
     * @return mixed
     */
    public function getRedirect(): mixed
    {
        return $this->redirect;
    }

    /**
     * @param mixed $redirect
     */
    public function setRedirect($redirect)
    {
        $this->redirect = $redirect;
    }

    /**
     * @return array
     */
    public function getSort(): array
    {
        return $this->sort;
    }

    /**
     * @param array $sort
     */
    public function setSort(array $sort): void
    {
        $this->sort = $sort;
    }
}