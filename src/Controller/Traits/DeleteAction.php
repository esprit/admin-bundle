<?php

namespace AdminBundle\Controller\Traits;

use AdminBundle\Configuration\CrudAction;
use Symfony\Component\Routing\Annotation\Route;

trait DeleteAction
{
    #[Route("/{id}/delete", name: "@delete")]
    #[CrudAction("delete")]
    public function deleteAction()
    {
        return [];
    }
}