<?php

namespace AdminBundle\Controller\Traits;

use AdminBundle\Configuration\CrudAction;
use Symfony\Component\Routing\Annotation\Route;

trait NewAction
{
    #[Route("/new", name: "@new")]
    #[CrudAction("new")]
    public function newAction()
    {
        return [];
    }
}