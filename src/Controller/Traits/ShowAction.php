<?php

namespace AdminBundle\Controller\Traits;

use AdminBundle\Configuration\CrudAction;
use Symfony\Component\Routing\Annotation\Route;

trait ShowAction
{
    #[Route("/{id}/show", name: "@show")]
    #[CrudAction("show")]
    public function showAction()
    {
        return [];
    }
}