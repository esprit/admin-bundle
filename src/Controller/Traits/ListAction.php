<?php

namespace AdminBundle\Controller\Traits;

use AdminBundle\Configuration\CrudAction;
use Symfony\Component\Routing\Annotation\Route;

trait ListAction
{
    #[Route("", name: "@index")]
    #[CrudAction("index")]
    public function indexAction()
    {
        return [];
    }
}