<?php

namespace AdminBundle\Controller\Traits;

use AdminBundle\Configuration\CrudAction;
use Symfony\Component\Routing\Annotation\Route;

trait EditAction
{
    #[Route("/{id}/edit", name: "@edit")]
    #[CrudAction("edit")]
    public function editAction()
    {
        return [];
    }
}