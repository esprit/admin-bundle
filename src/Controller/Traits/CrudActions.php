<?php

namespace AdminBundle\Controller\Traits;

trait CrudActions
{
    use ListAction;
    use NewAction;
    use EditAction;
    use DeleteAction;
    use ShowAction;
}