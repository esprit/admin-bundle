<?php

namespace AdminBundle\Controller;

use AdminBundle\Menu\MenuBuilder;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\RequestStack;

class DefaultController
{
    /**
     * @Template("@Admin/Default/menu.html.twig")
     */
    public function menu(MenuBuilder $menuBuilder)
    {
        return ['menu' => $menuBuilder->build()];
    }
}