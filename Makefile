
.PHONY: all
all: check test

.PHONY: check
check:
	@vendor/bin/phpstan analyse --no-progress

.PHONY: test
test:
	@vendor/bin/phpunit tests