AdminBundle
===================

AdminBundle lets you create administration backend for Symfony applications.

**Features**

  * CRUD operations (list, new, edit, delete).
  * Full-text search, pagination and column sorting.
  * Simple and smart!
  * And many more...

Examples
--------
```php
/**
 * @Route("/blog", name="blog")
 * @CrudAction(repository="AppBundle:BlogPost", fields={
 *     "id|link('@show', entity)",
 *     "title|truncate(25)"
 * })
 */
class BlogPostController extends Controller
{
    use CrudActions;
}
```
![Kiku](src/Resources/doc/images/blog1.png)

Compatibility
--------
    tip  = symfony 4.3
    1.5  - symfony 4.2
    1.4  - symfony 4.1
    1.3  - symfony 4.0
    1.2  - symfony 3.4

Changelog
--------
default

- remove deprecated options (https://symfony.com/blog/new-in-symfony-4-3-routing-improvements#deprecated-some-router-options)
- extends custom UrlGenerator from symfony's CompiledUrlGenerator

1.5

- add ```$options``` argument in FormGenerator::create

1.4

- use ```@index``` route instead of ```@```
- support symfony 4.1
- NOTE: generateUrl from object is not supported anymore (because of typehint in symfony Controller)
   
1.3.1

- support ```@Embedded``` form generation
- get form name from ```@Form``` annotation
- new form-generator

1.3.0

- support for symfony 4
- removed builtin bootstrap theme

1.2 (sf3 branch)

- rename ```list``` option into ```fields```
- build form from fields, add form type guesser
- removed braincrafted/bootstrap-bundle

How to migrate from braincrafted/bootstrap-bundle:

    * use form_theme from symfony
    * replace flash template 
    * replace BootstrapCollectionType by custom collection type
    * replace ```input_group``` attributes with custom form types
    * show ```helper_text``` attribute showing

2017 Mart

- rename `form_widget` block to `form_fields`